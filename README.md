# stocktwits 
[![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-url]][daviddm-image] [![Coverage Status][coveralls-image]][coveralls-url]

The best module ever.


## Install

```bash
$ npm install --save stocktwits
```


## Usage

```javascript
var stocktwits = require('stocktwits');
stocktwits(); // "awesome"
```

## API

_(Coming soon)_


## Contributing

In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [gulp](http://gulpjs.com/).


## License

Copyright (c) 2015. Licensed under the MIT license.



[npm-url]: https://npmjs.org/package/stocktwits
[npm-image]: https://badge.fury.io/js/stocktwits.svg
[travis-url]: https://travis-ci.org/user/stocktwits
[travis-image]: https://travis-ci.org/user/stocktwits.svg?branch=master
[daviddm-url]: https://david-dm.org/user/stocktwits.svg?theme=shields.io
[daviddm-image]: https://david-dm.org/user/stocktwits
[coveralls-url]: https://coveralls.io/r/user/stocktwits
[coveralls-image]: https://coveralls.io/repos/user/stocktwits/badge.png
