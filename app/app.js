/*
 * stocktwits
 * user/repo
 *
 * Copyright (c) 2015
 * Licensed under the MIT license.
 */

'use strict';

var routes = require('../lib/stocktwits/api/routes');
var server = require('../lib/utils/server');

//stocktwits.authenticate();
console.log("start server");
server.addRoutes(routes.routes);
server.start();
