"use strict";

module.exports = {
  setState : function(context, stateAttributesMap){
    var state = context.state;

    for(var k in stateAttributesMap) {
      state[k] = stateAttributesMap[k];
    }
    context.setState(state);
  }
};
