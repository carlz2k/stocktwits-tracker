"use strict";
var alt = require('./AltInit');
module.exports = alt.createActions({
  addTrendingSymbols: function () {
    this.dispatch();
  },
  getTrendingMessages: function() {
    console.log("get trending called once");
    var self = this;
    setInterval(function () {
      self.dispatch();
    }, 10*60*1000);
  },
  getTrendingSymbols: function() {
    console.log("get trending called once");
    var self = this;
    setInterval(function () {
      self.dispatch();
    }, 10*60*1000);
  },

  getAllTwits: function(symbol) {
    console.log("get symbol twits once");
    this.dispatch(symbol);
  }
});
