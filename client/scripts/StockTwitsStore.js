"use strict";
var React = require("react");
var alt = require('./AltInit');
var actions = require('./StockTwitsActions');
var StocktwitsDataService = require('./StocktwitsDataService');
var Immutable = require('immutable');
var _ = require('lodash');

function getTrendingMessages(context) {
  StocktwitsDataService.getTrendingMessages(function (trendings) {
    context.setState({
      trendingMessages: trendings
    });
  }, function (err) {
    console.log("get stream trendings error: " + err);
    context.setState({
      trendingMessages: []
    });
  });
}

function getTrendingSymbols(context) {
  StocktwitsDataService.getTrendingSymbols(function (trendings) {
    context.setState({
      trendingSymbols: trendings
    });
    console.log("request twits for stocks");
    _.each(trendings, function (trending) {
      getAllTwits(context, trending.symbol);
    });
  }, function (err) {
    console.log("get stream trendings error: " + err);
    context.setState({
      trendingSymbols: []
    });
  });
}

function getAllTwits(context, symbol) {
  StocktwitsDataService.getAllTwitsForSymbol(symbol,
    function (allTwits) {
      context.setState({
        twits: updateSymbolTwits(context, symbol, allTwits)
      });
    }, function (err) {
      console.log("get all twtis for" + symbol + " error: " + err);
      context.setState({
        twits: updateSymbolTwits(context, symbol, [])
      });
    });
}

function updateSymbolTwits(context, symbol, allTwits) {
  var symbolTwitsMapping = context.state.twits || Immutable.Map();
  return symbolTwitsMapping.set(symbol, allTwits);
}

module.exports = alt.createStore({
  displayName: 'StockTwitsStore',

  bindListeners: {
    addTrendingSymbols: actions.addTrendingSymbols,
    getTrendingMessages: actions.getTrendingMessages,
    getTrendingSymbols: actions.getTrendingSymbols,
    getAllTwits: actions.getAllTwits
  },

  state: {
    trendingMessages: [],
    trendingSymbols: [],
    twits: Immutable.Map()
  },

  addTrendingSymbols: function () {
    getTrendingSymbols(this);
  },

  getTrendingMessages: function () {
    getTrendingMessages(this);
  },

  getTrendingSymbols: function () {
    getTrendingSymbols(this);
  },

  getAllTwits: function (symbol) {
    getAllTwits(this, symbol);
  },

  pubilcMethods: {}
});
