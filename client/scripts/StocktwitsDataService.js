"use strict";
var request = require('superagent');
var serverHost = "http://van-ws012:3000";

function doGet(url, success, error) {
  var TIMEOUT = 20*1000;
  var cachebuster = Math.round(new Date().getTime() / 1000);
  request
    .get(serverHost+url)
    .timeout(TIMEOUT)
    .set('Accept', 'application/json')
    .query({ nocache: cachebuster }) //temporary for cache busting
//    .use(nocache)
    .end(function(err, res){
      if(err) {
        error(err);
      } else {
        success(res.body);
      }
    });
}
var StocktwtisDataService = {
  getTrendingMessages : function(success, error) {
    doGet('/trendingmessages/30', success, error);
  },
  getTrendingSymbols : function(success, error) {
    doGet('/trendingsymbols/30', success, error);
  },
  getAllTwitsForSymbol : function(symbol, success, error) {
    doGet('/getalltwits/'+symbol+'/30', success, error);
  }
};

module.exports = StocktwtisDataService;
