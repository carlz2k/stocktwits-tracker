"use srict";
var React = window.React = require('react');
var injectTapEventPlugin = require("react-tap-event-plugin");
var router = require('./router');

window.React = React;
injectTapEventPlugin();

router.run(function (Handler) {
    React.render(<Handler />, document.body);
});
