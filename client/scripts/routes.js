var MaterialUi = require('./ui/MaterialMain'),
    BootstrapUi = require('./ui/ReactBootstrapMain'),
    MainBody = require('./ui/MainBody');
var { Route, DefaultRoute } = require('react-router');

module.exports = (
    <Route name="app"  path="/" handler={MainBody}>
        <DefaultRoute handler={BootstrapUi}/>
        <Route name="bootstrap"  handler={BootstrapUi}/>
        <Route name="material" handler={MaterialUi}/>
    </Route>
    );
