"use strict";
var React = require('react');
var
  ListGroup = require('react-bootstrap').ListGroup,
  ListGroupItem = require('react-bootstrap').ListGroupItem;

var List = React.createClass({

  render: function () {

    return (
      <ListGroup>
        {
          this.props.list.map(function (item) {
            return <ListGroupItem>{item}</ListGroupItem>
          })
        }
      </ListGroup>
    );
  }
});

module.exports = List;
