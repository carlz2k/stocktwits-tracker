"use strict";
var Router = require('react-router');
var { RouteHandler, Link } = Router;

module.exports = React.createClass({
  render: function () {
    return (
      <div>
        <div className={"header"}>
          <ul className={'nav nav-pills pull-right'}>
            <li>
              <Link to="bootstrap">Bootstrap Test</Link>
            </li>
            <li>
              <Link to="material">Material Ui Test</Link>
            </li>
          </ul>
          <h3 className={"text-muted"}>reacttest</h3>
        </div>
        <div id="content" className={"row marketing"}>
          <RouteHandler />
        </div>
      </div>
    );
  }
});
