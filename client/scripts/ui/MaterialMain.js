var React = require('react'),
  mui = require('material-ui'),
  RaisedButton = mui.RaisedButton,
  DatePicker = mui.DatePicker,
  SelectUi = require('./SelectComponent'),
  ThemeManager = new mui.Styles.ThemeManager(),
  Colors = mui.Styles.Colors;

var MaterialMain = React.createClass({
  childContextTypes: {
    muiTheme: React.PropTypes.object
  },

  getChildContext: function () {
    return {
      muiTheme: ThemeManager.getCurrentTheme()
    };
  },

  componentWillMount: function () {
    ThemeManager.setPalette({
      accent1Color: Colors.deepOrange500
    });
  },

  render: function () {

    var options = [
      {
        payload: 1,
        text: 'Man Utd'
      },
      {
        payload: 2,
        text: 'Chelsea'
      },
      {
        payload:3,
        text:'Arsenal'
      }
    ];

    return (
      <div>
        <table>
          <tr>
            <td><i className="fa fa-calendar fa-2x"></i>
            </td>
            <td>
              <DatePicker hintText="Landscape Dialog" mode="landscape" ref="testDate"
                          defaultDate={new Date()} />
            </td>
            <td><i className="fa fa-calendar fa-2x"></i>
            </td>
            <td>
              <DatePicker hintText="Landscape Dialog" mode="landscape" ref="testDate1"
                          defaultDate={new Date()} />
            </td>
          </tr>
        </table>
        <SelectUi options={options} />
        <RaisedButton label="Test Button"
                      secondary={true} onTouchTap={this._handleTouchTap} />
      </div>

    );
  },
  _handleTouchTap: function () {
    alert(this.refs.testDate.getDate());
  }

});

module.exports = MaterialMain;
