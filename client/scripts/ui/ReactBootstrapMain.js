"use strict";
var React = require('react');
var Modal = require('react-bootstrap').Modal,
  Button = require('react-bootstrap').Button,
  Popover = require('react-bootstrap').Popover,
  Tooltip = require('react-bootstrap').Tooltip,
  OverlayTrigger = require('react-bootstrap').OverlayTrigger,
  StockList = require('./StockList');

var ListenerMixin = require('alt/mixins/ListenerMixin');
var stockTwitsStore = require('../StockTwitsStore');
var stockTwitsAction = require('../StockTwitsActions');
var StateUtils = require('../StateUtils');
var _ = require('lodash');
var S = require('string');
var Immutable = require('immutable');

function convertTrendingSymbols(original) {
  var trendings = Immutable.List();
  _.each(original, function (t) {
    if (t) {
      var Record = Immutable.Record({
        title: S(t.title).decodeHTMLEntities().s,
        symbol: t.symbol
      });
      var trending = new Record({});
      trendings = trendings.push(trending);
    }
  });

  return trendings;
}

function convertTrendingMessages(original) {
  var trendings = Immutable.List();
  _.each(original, function (t) {
    if (t) {
      var Record = Immutable.Record({
        content: S(t.body).decodeHTMLEntities().s,
        symbols: t.symbols ? t.symbols : []
      });
      var trending = new Record({});
      trendings = trendings.push(trending);
    }
  });

  return trendings;
}

var ReactBootstrapMain = React.createClass({
  mixins: [ListenerMixin],

  getInitialState: function () {
    stockTwitsAction.getTrendingSymbols();
    return {
      showModal: false,
      trendingSymbols: convertTrendingSymbols(stockTwitsStore.getState().trendingSymbols)
    };
  },
  close: function () {
    StateUtils.setState(this, {
      showModal: false
    });
  },
  open: function () {
    StateUtils.setState(this, {
      showModal: true
    });
  },

  onAddTrendingSymbols: function (e) {
    e.preventDefault();
    stockTwitsAction.addTrendingSymbols();
  },

  componentWillMount() {
    var self = this;
    this.listenTo(stockTwitsStore, function () {
      StateUtils.setState(self, {
        trendingSymbols: convertTrendingSymbols(stockTwitsStore.getState().trendingSymbols)
      });
    });
  },

  render: function () {

    let popover = <Popover title='popover'>very popover. such engagement</Popover>;
    let tooltip = <Tooltip>wow.</Tooltip>;

    return (
      <div>
        <p>TRENDINGS</p>

        <div>
          <StockList stocks={this.state.trendingSymbols}>
          </StockList>
        </div>
        <p>Click to get the full Modal experience!</p>

        <Button
          bsStyle='primary'
          bsSize='large'
          onClick={this.onAddTrendingSymbols}
          >Load trends
        </Button>

        <Button
          bsStyle='primary'
          bsSize='large'
          onClick={this.open}
          >
          Launch demo modal
        </Button>

        <Modal show={this.state.showModal} onHide={this.close}>
          <Modal.Header closeButton>
            <Modal.Title>Modal heading</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h4>Text in a modal</h4>

            <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula.</p>

            <h4>Popover in a modal</h4>

            <p>there is a <OverlayTrigger overlay={popover}><a href='#'>popover</a></OverlayTrigger>
              here</p>

            <h4>Tooltips in a modal</h4>

            <p>there is a <OverlayTrigger overlay={tooltip}><a href='#'>tooltip</a></OverlayTrigger>
              here</p>

            <hr />

            <h4>Overflowing text to show scroll behavior</h4>

            <p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac
              facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum
              at eros.</p>

            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis
              lacus vel augue laoreet rutrum faucibus dolor auctor.</p>

            <p>Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel
              scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non
              metus auctor fringilla.</p>

            <p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac
              facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum
              at eros.</p>

            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis
              lacus vel augue laoreet rutrum faucibus dolor auctor.</p>

            <p>Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel
              scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non
              metus auctor fringilla.</p>

            <p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac
              facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum
              at eros.</p>

            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis
              lacus vel augue laoreet rutrum faucibus dolor auctor.</p>

            <p>Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel
              scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non
              metus auctor fringilla.</p>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.close}>Close</Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
});

module.exports = ReactBootstrapMain;
