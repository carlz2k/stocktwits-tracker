"use strict";
var React = require('react');
var mui = require('material-ui'),
  DropDownMenu = mui.DropDownMenu;
var _ = require('lodash');

function getIndex(options, value, propertyName) {
  for (var i = 0; i < options.length; i++) {
    if (options[i][propertyName] === value) {
      return i;
    }
  }
  return 0;
}

module.exports = React.createClass({
  getInitialState: function () {
    var index = 0;
    if(this.props.options && this.props.defaultSelectedValue) {
      index = getIndex(this.props.options, this.props.defaultSelectedValue, 'payload');
    }
    return {selectedIndex: index};
  },
  handleChange: function (e, selectedIndex, menuItem) {
    this.setState({selectedIndex: getIndex(this.props.options, menuItem.payload, 'payload')});
  },
  render: function () {

    return (
      <div className="bandOption">
        <label>Band</label>
        <DropDownMenu menuItems={this.props.options}
                      selectedIndex={this.state.selectedIndex}
                      onChange={this.handleChange} />
      </div>
    );
  }
});
