"use strict";
var React = require('react');
var
  ListGroup = require('react-bootstrap').ListGroup,
  ListGroupItem = require('react-bootstrap').ListGroupItem;

var stockTwitsStore = require('../StockTwitsStore');
var stockTwitsAction = require('../StockTwitsActions');
var StateUtils = require('../StateUtils');
var ListenerMixin = require('alt/mixins/ListenerMixin');
var ListGroup = require('react-bootstrap').ListGroup,
  ListGroupItem = require('react-bootstrap').ListGroupItem,
  StockTwits = require('./StockTwits');

var StockList = React.createClass({

  render: function () {
    return (
      <ListGroup>
        {
          this.props.stocks.map(function (trending) {
            return <ListGroupItem>{formatTrendingSymbols(trending)}<StockTwits
              symbol={trending.symbol}></StockTwits></ListGroupItem>
          })
        }
      </ListGroup>
    );

    function formatTrendingSymbols(trending) {
      var trendingSymbol = "";
      if (trending) {
        trendingSymbol = "(" + trending.symbol + ", " + trending.title + ")";
      }
      return trendingSymbol;
    }

  }
});

module.exports = StockList;
