"use strict";
var React = require('react');
var List = require('./List');

var stockTwitsStore = require('../StockTwitsStore');
var stockTwitsAction = require('../StockTwitsActions');
var StateUtils = require('../StateUtils');
var ListenerMixin = require('alt/mixins/ListenerMixin');

function getAllTwits(symbol) {
  return stockTwitsStore.getState().twits.get(symbol) || [];
}

var StockTwits = React.createClass({
  mixins: [ListenerMixin],

  getInitialState: function () {
    return {
      twits: getAllTwits(this.props.symbol)
    };
    //var symbol = this.props.symbol;
    //stockTwitsAction.getAllTwits(symbol);
    //return {
    //  twits: getAllTwits(symbol)
    //};
  },

  componentWillMount() {
    var symbol = this.props.symbol;
    var self = this;
    this.listenTo(stockTwitsStore, function () {
      StateUtils.setState(self, {
        twits: getAllTwits(symbol)
      });
    });
  },

  render: function () {
    return (
      <List list={this.state.twits}></List>
    );
  }
});

module.exports = StockTwits;
