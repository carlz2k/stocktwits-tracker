'use strict';

var gulp = require('gulp');
var nodemon = require('gulp-nodemon');

gulp.task('server-run', function () {
  nodemon({
    script: 'app/app.js'
    , watch: ['lib','app']
  })
});
