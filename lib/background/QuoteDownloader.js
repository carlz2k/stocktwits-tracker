'use strict';

var stockFeedDownloadService = require('stock-quote-downloader').stockFeedDownloadService;
var symbolDownloadService = require('stock-quote-downloader').symbolDownloadService;
var StockDao = require('../persistence/mongoose/dao/StockDao');
var Stock = require('../persistence/mongoose/model/Stock');
var MongooseUtil = require('../persistence/mongoose/MongooseUtil');
var _ = require('lodash');

var downloadStockQuotes = symbol => {
  symbolDownloadService.getSymbols(symbol).then(function (companies) {
    _.each(companies, function (company) {
      if (company) {
        stockFeedDownloadService.getEodQuote(company.symbol, company.market)
          .then(function (quote) {
            console.log("stock processsed " + quote.symbol);
            var stock = new Stock();
            stock.symbol = quote.symbol;
            stock.weeksHigh52 = quote.yearHigh;
            stock.weeksLow52 = quote.yearLow;
            stock.priceEarnings = quote.priceEarningRatio;
            stock.close = quote.close;
            stock.volume = quote.volume;
            stock.averageVolume = quote.averageVolume;
            stock.name = company.name;
            return stock;
          }).then(function (stock) {
            return StockDao.save(stock);
          }).then(function (stock) {
            console.log("stock saved " + stock.symbol);
          }).catch(function (error) {
            console.log("error when download quote for " + company.symbol+" "+error);
          });
      }
    }).catch(function (error) {
      console.log("error: " + error);
    });
  });
};

MongooseUtil.connect();

downloadStockQuotes("nasdaq");
downloadStockQuotes("amex");
downloadStockQuotes("nyse");
