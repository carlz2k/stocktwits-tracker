"use strict";
var _ = require('lodash');
var StockDao = require('../persistence/mongoose/dao/StockDao');

var wm = new WeakMap();
wm.set({}, { extra: 42 });
console.log("size = "+wm.size);

StockDao.search().then(function(result){
  _.each(result, function(stock){
    console.log("stock = " + stock.symbol + " "+stock.name);
  });
});
