'use strict';

var mongoose = require('mongoose');
var promise = require('promise');

module.exports = class MongooseUtil {
  static connect() {
    mongoose.connect('mongodb://localhost/stock');
  }

  static disconnect() {
    mongoose.disconnect();
  }

  static save(obj) {
    return new promise(function (resolve, reject) {
      obj.save(function (err) {
        if (err) {
          reject({
            error: err
          });
        } else {
          resolve(obj._doc);
        }
      });
    });
  }
};
