'use strict';

var MongooseUtil = require('../MongooseUtil');
var StockDaoQuery = require('./StockDaoQuery');
var Stock = require('../model/Stock');
var _ = require('lodash');
var promise = require('promise');

class StockDao {
  static save(stock) {
    var stockDaoQuery = new StockDaoQuery(Stock.StockModel);
    return stockDaoQuery.findOne(stock.symbol).execute().then(function(resultDoc){
      if (resultDoc && resultDoc.length) {
        var found = resultDoc[0];
        stock.updateDbModel(found);
        return MongooseUtil.save(found);
      } else {
        return MongooseUtil.save(stock.toDbModel());
      }
    });
  }

  static search() {
    MongooseUtil.connect();
    var stockDaoQuery = new StockDaoQuery(Stock.StockModel);
    return stockDaoQuery.addPriceRange(100, 3).addPERatioRange(25, 0.1).addVolumeFilter().addPriceFilter().execute().then(function (resultDocs) {
      MongooseUtil.disconnect();
      var result = [];
      _.each(resultDocs, function (resultDoc) {
        result.push(resultDoc._doc);
      });
      return result;
    });
  }
}

module.exports = StockDao;
