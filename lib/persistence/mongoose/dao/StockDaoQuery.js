'use strict';

var addRange = (query, columnName, max, min) => {
  if (max) {
    query = query.where(columnName).lt(max);
  }

  if (min) {
    query = query.where(columnName).gt(min);
  }

  return query;
};

class StockDaoQuery  {

  constructor(stockModel) {
    this.query = stockModel;
  }

  addPriceRange(max, min) {
    this.query = addRange(this.query,'close', max, min);
    return this;
  }

  addPERatioRange(max, min) {
    this.query = addRange(this.query,'priceEarnings', max, min);
    return this;
  }

  addVolumeFilter() {
    this.query = this.query.$where('this.volume > this.averageVolume * 2');
    return this;
  }

  addPriceFilter() {
    this.query = this.query.$where('this.weeksHigh52 > this.close * 2');
    return this;
  }

  findOne(symbol) {
    this.query = this.query.where({symbol: symbol});
    return this;
  }

  execute() {
    return this.query.exec();
  }
}


module.exports = StockDaoQuery;
