"use strict";
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var StockSchema = new Schema({
  symbol: {type: String, index: {unique: true}},
  weeksHigh52: Number,
  weeksLow52: Number,
  priceEarnings: Number,
  close: Number,
  volume: Number,
  averageVolume: Number,
  name: String
});

var Stock = function () {
  this.symbol = '';
  this.weeksHigh52 = 0;
  this.weeksLow52 = 0;
  this.priceEarnings = 0;
  this.close = 0;
  this.volume = 0;
  this.averageVolume = 0;
  this.name = '';
};

Stock.StockModel = mongoose.model('Stock', StockSchema);

Stock.prototype.toDbModel = function () {
  return new Stock.StockModel({
    symbol: this.symbol,
    close: this.close,
    weeksHigh52: this.weeksHigh52,
    weeksLow52: this.weeksLow52,
    priceEarnings: this.priceEarnings,
    volume: this.volume,
    averageVolume: this.averageVolume,
    name: this.name
  });
};

Stock.prototype.updateDbModel = function (dbModel) {
  dbModel.close = this.close;
  dbModel.weeksHigh52 = this.weeksHigh52;
  dbModel.weeksLow52 = this.weeksLow52;
  dbModel.priceEarnings = this.priceEarnings;
  dbModel.volume = this.volume;
  dbModel.averageVolume = this.averageVolume;
};

module.exports = Stock;
