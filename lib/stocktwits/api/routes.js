'use strict';

var stocktwits = require('./stocktwits');

function handleError(promise, reply) {
  return promise.catch(function (error) {
    reply(error.message).code(500);
  });
}
module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/',
      handler: function (request, reply) {
        reply('stocktwits!');
      }
    }, {
      method: 'GET',
      path: '/trendingsymbols/{limit}',
      handler: function (request, reply) {
        handleError(stocktwits.getTrendingSymbols(request.params.limit).then(function (symbols) {
          reply(symbols);
        }), reply);
      }
    }, {
      method: 'GET',
      path: '/trendingmessages/{limit}',
      handler: function (request, reply) {
        console.log("api6 get streamtreadings " + new Date());
        stocktwits.getTrendingMessages(request.params.limit).then(function (messages) {
          reply(messages);
        });
      }
    }, {
      method: 'GET',
      path: '/getalltwits/{symbol}/{limit}',
      handler: function (request, reply) {
        var symbol = request.params.symbol;
        console.log("api6 get all twits " + symbol + new Date());
        handleError(stocktwits.getAllTwitsForSymbol(symbol,
          request.params.limit).then(function (response) {
            reply(response);
          }), reply);
      }
    }
  ]
};

