/*
 * stocktwits
 * user/repo
 *
 * Copyright (c) 2015
 * Licensed under the MIT license.
 */

'use strict';
var httpclient = require('../../utils/httpclient');
//var phantom = require('node-phantom');
//var htmlparser = require('../../utils/htmlparser');
//var Browser = require('zombie');
var logger = require('../../utils/logger');
var _ = require('lodash');

function submitForm() {

  //var browser = new Browser();
  //browser.visit(url, function (status) {
  //  browser.on('error', function (err) {
  //    console.log(err);
  //  });
  //  browser.on('done', function (done) {
  //    console.log(done);
  //  });
  //  browser
  //    .fill('input[id="user_session_login"]', 'zhangcarlster@gmail.com')
  //    .fill('input[id="user_session_password"]', 'carlz2k')
  //    .pressButton('input[name="commit"]');
  //});
}

function handleError(promise) {
  return promise.catch(
    function (err) {
      var errorMessage = err;
      if (err.error) {
        errorMessage = err.error;
      } else if (err.httpResponse && err.httpResponse.body) {
        errorMessage = err.httpResponse.body;
      } else if (err && err.message) {
        errorMessage = err.message;
      }
      throw new Error(errorMessage);
    });
}
var api = {

  authenticate: function () {
    httpclient.formPost('https://api.stocktwits.com/api/2/oauth/authorize', {
      client_id: 'b70cce3b0ccb1fba',
      response_type: 'code',
      prompt: '0',
      scope: 'read,watch_lists,publish_messages,publish_watch_lists,follow_users,follow_stocks',
      redirect_uri: 'http://www.rainbowbridgeenglish.com'
    }).then(function () {
      return ""; //htmlparser.parseHtml(result.body);
    }).then(
      function (window) {
        var redirectUrl = window.document.getElementsByTagName('a')[0].href;
        window.close();
        submitForm(redirectUrl);
      }).catch(
      function (err) {
        console.log(err.error);
      });
  },

  getTrendingSymbols: function (maxNumberOfSymbols) {
    return handleError(httpclient.jsonGet('https://api.stocktwits.com/api/2/trending/symbols.json',
      {limit: maxNumberOfSymbols})
      .then(
      function (result) {
        logger.debug("start!");
        logger.debug("get trending result, first element = ", result.body.symbols[0]);
        return result.body.symbols;
      }));
  },
  getTrendingMessages: function (maxNumberOfSymbols) {
    return handleError(httpclient.jsonGet('https://api.stocktwits.com/api/2/streams/trending.json',
      {limit: maxNumberOfSymbols})
      .then(
      function (result) {
        return result.body.messages;
      }));
  },
  getAllTwitsForSymbol: function (symbol, limit) {
    return handleError(httpclient.jsonGet('https://api.stocktwits.com/api/2/streams/symbol/'+
      symbol+'.json', {limit: limit})
      .then(
      function (result) {
        var messages = [];
        _.each(result.body.messages, function(message){
          var body = message.body;
          if(body) {
            messages.push(body);
          }
        });
        return messages;
      }));
  }
};

module.exports = api;
