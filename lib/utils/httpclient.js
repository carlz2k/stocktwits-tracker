'use strict';
var request = require('request');
var promise = require('promise');
var queryString = require('querystring');
var _ = require('lodash');

function getResponseHandler(resolve, reject) {
  return function (err, httpResponse, body) {
    if (err ||
      (httpResponse && (httpResponse.statusCode !== 200 && httpResponse.statusCode !== 302))) {
      reject({
        error: err,
        httpResponse: httpResponse
      });
    } else {
      resolve({
        httpResponse: httpResponse,
        body: body
      });
    }
  };
}

function createOptions(extraOptions) {
  var TIMEOUT = 20*1000;
  return _.extend(extraOptions, {timeout:TIMEOUT});
}

var httpclient = {
  get: function (url, callbacks) {
    request.get(createOptions({
      url: url
    }), callbacks);
  },
  formPost: function (url, params) {
    return new promise(function (resolve, reject) {
      request.post(createOptions({
          url: url,
          formData: params
        }),
        getResponseHandler(resolve, reject)
      );
    });
  },
  jsonGet: function (url, queryParams) {
    if(queryParams) {
      url = url+'?'+queryString.stringify(queryParams);
    }
    return new promise(function (resolve, reject) {
      request.get(createOptions({
        url: url,
        json: true
      }), getResponseHandler(resolve, reject));
    });
  }
};
module.exports = httpclient;
