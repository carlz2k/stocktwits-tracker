'use strict';
var Hapi = require('hapi');
var _ = require('lodash');

var server = new Hapi.Server();
server.connection({port: 3000, routes: { cors: true }});

module.exports = {
  addRoutes: function (routes) {
    _.each(routes, function (rule) {
      server.route(rule);
    });
  },
  start: function () {
    server.start(
      function () {
        console.log('Server running at:', server.info.uri);
      }
    );
  }
};
