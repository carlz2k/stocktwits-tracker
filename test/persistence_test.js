'use strict';
var chai = require('chai');
chai.config.includeStack = true;
chai.use(require('chai-as-promised'));

var MongooseUtil = require('../lib/persistence/mongoose/MongooseUtil');
var StockDao = require('../lib/persistence/mongoose/dao/StockDao');
var Stock = require('../lib/persistence/mongoose/model/Stock');


describe('stock model mongoose test', function () {
  before(function() {
    MongooseUtil.connect();
  });

  after(function() {

  });

  it('save stock', function() {
    var stock = new Stock();
    stock.symbol = "INTC";
    stock.priceEarnings=12.5;
    stock.weeksHigh52 = 32;
    stock.weeksLow52 = 28;
    StockDao.save(stock).then(function(){
      console.log("success");
      MongooseUtil.disconnect();
    }).catch( function(){
      console.log("fail");
      MongooseUtil.disconnect();
    });
  });
});


