'use strict';
var chai = require('chai');
chai.config.includeStack = true;
chai.use(require('chai-as-promised'));

//var expect = chai.expect;
var should = chai.should();
//var AssertionError = chai.AssertionError;
//var Assertion = chai.Assertion;
//var assert = chai.assert;

var stocktwits = require('../lib/stocktwits/api/stocktwits');

describe('stocktwits get symbol streams', function () {
  it('get csco stream', function() {
    var limit = 20;
    stocktwits.getAllTwitsForSymbol("csco", limit).should.eventually.have.length(limit);
    return true;
  });
  it('length of the response equals limit', function() {
    var limit = 20;
    return stocktwits.getTrendingSymbols(limit).should.eventually.have.length(limit);
  });
  it('length of the response equals 30', function() {
    var limit = 50;
    return stocktwits.getTrendingSymbols(limit).should.eventually.have.length(30);
  });

  it('length of the response equals 30', function() {
    var limit = 30;
    return stocktwits.getAllTwitsForSymbol("SPY", limit).should.eventually.have.length(limit);
  });
});
